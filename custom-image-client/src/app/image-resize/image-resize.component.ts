import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ImageResizeService }  from './image-resize.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/find';
import { environment } from '../../environments/environment';
import { UUID } from 'angular2-uuid';

@Component({
    templateUrl: 'image-resize.component.html'    
})
export class ImageResizeComponent implements OnInit {

    isResizing: boolean = false;

    images: any[] = [];

    constructor(
        private service: ImageResizeService,
        private route: ActivatedRoute,
        private router: Router,
    ) {}

    ngOnInit() {
        this.images = [];        
        this.isResizing = false;

        /*
        let uploadedImageNames: string[] = this.buffer.getValue("uploadedImages");
        this.images = uploadedImageNames ? uploadedImageNames.map(imageName => {
            return {
                name: imageName,
                url: environment.apiServerUrl + '/download/' + imageName,
                thumbnailUrl: this.THUMBNAIL_STUB_URL
            }
        }) : [];

        // consider sending chunked requests as an alternative to sending one by one and by entire scope
        for (let image of this.images) {
            let thumbnailRequest = this.createThumbnailRequest(image);
            this.getThumbnails([thumbnailRequest]);
        }
        */
    }

    addImages($event: any) { 
        console.log('Adding images...');
        let rawFiles = $event.target.files;
        if (rawFiles && rawFiles.length > 0) {
            for (let rawFile of rawFiles) {
                let image = {
                    id: UUID.UUID(),
                    thumbnail: null,
                    file: rawFile,
                    downloadUrl: null                    
                }
                this.images.push(image);
                let reader = new FileReader();
                reader.onload = (e: any) => {
                    image.thumbnail = e.target.result;
                }                
                reader.readAsDataURL(rawFile);
            }            
        } 
    }

    removeImage(imageId: string) {
        this.images = this.images.filter(item => item.id !== imageId);
    }

    resizeImages() {
        let resizePayload = this.images.map(image => {
            return {
                imageId: image.id,
                actions: [{
                    actionName: "scale",
                    options: [0.5]
                }]
            }
        });
        this.service.getToken(resizePayload).subscribe(json => {
            let token = json.data;

            this.service.uploadImages(token, this.images).subscribe(json => {

                let resizedImages: any[] = json.data.images;
                for (let resizedImage of resizedImages) {                    
                    let originalImage = this.images.find(image => image.id === resizedImage.imageId);
                    if (!!originalImage) {
                        originalImage.downloadUrl = resizedImage.downloadUrl;                        
                    }
                }
            });
        });
    }    
}