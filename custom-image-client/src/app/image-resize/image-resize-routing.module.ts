import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageResizeComponent }    from './image-resize.component';

const imageResizeRoutes: Routes = [
  { path: '',  component: ImageResizeComponent }//,
  //{ path: 'hero/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(imageResizeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ImageResizeRoutingModule { }


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/