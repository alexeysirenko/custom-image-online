import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { ImageResizeComponent }    from './image-resize.component';
import { ImageResizeService } from './image-resize.service';
import { ImageResizeRoutingModule } from './image-resize-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ImageResizeRoutingModule
  ],
  declarations: [
    ImageResizeComponent
  ],
  providers: [ ImageResizeService ]
})
export class ImageResizeModule {}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/