import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';


@Injectable()
export class ImageResizeService {

    readonly uploadUrl = environment.apiServerUrl + "/api/resize/upload/";

    readonly tokenUrl = environment.apiServerUrl + "/api/resize/token";
    
    constructor(private http: Http) {
        this.http = http;
    }

    getToken(resizePayload) {
        return this.http.post(this.tokenUrl, resizePayload)
            .map(res => res.json())
    }

    uploadImages(token: string, images: any[]) {
        let formData:FormData = new FormData();

        images.forEach(image => {
            formData.append(image.id, image.file, image.file.name);
        }); 

        let headers = new Headers({});
        /*headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');*/
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.uploadUrl + token, formData, options)
            .map(res => res.json())
            
    }
    
}