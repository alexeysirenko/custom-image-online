import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ImageUploadService }  from './image-upload.service';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';

@Component({
    templateUrl: 'image-upload.component.html'    
})
export class ImageUploadComponent implements OnInit {

    isUploading: boolean = false;

    imageUrls;

    constructor(
        private service: ImageUploadService,
        private route: ActivatedRoute,
        private router: Router,
    ) {}

    ngOnInit() {
        this.isUploading = false;
        this.imageUrls = [];
    }

    uploadImage($event: any) {  
        /*
        this.isUploading = true;      
        let rawFiles = $event.target.files;
        if (rawFiles && rawFiles.length > 0) {
            let files = [];
            for (var i = 0; i < rawFiles.length; i++) {
                files.push(rawFiles.item(i));
            }
            this.service.uploadImage(files).subscribe(response => {
                console.log(response.data);
                //this.buffer.setValue("uploadedImages", response.data);
                this.router.navigate(["/resize"]);
                //this.imageUrls = response.data.map(id => environment.apiServerUrl + "/download/" + id);
            },
            error => console.error(error),
            () => {
                //this.isUploading = false;
                // Redirect to image customization page
            })  
        } 
        */
    }

    
}