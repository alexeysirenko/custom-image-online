import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageUploadComponent }    from './image-upload.component';

const imageUploadRoutes: Routes = [
  { path: 'upload',  component: ImageUploadComponent }//,
  //{ path: 'hero/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(imageUploadRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ImageUploadRoutingModule { }


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/