import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { ImageUploadComponent }    from './image-upload.component';
import { ImageUploadService } from './image-upload.service';
import { ImageUploadRoutingModule } from './image-upload-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImageUploadRoutingModule,
    SharedModule
  ],
  declarations: [
    ImageUploadComponent
  ],
  providers: [ ImageUploadService ]
})
export class ImageUploadModule {}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/