import { CustomImageClientPage } from './app.po';

describe('custom-image-client App', function() {
  let page: CustomImageClientPage;

  beforeEach(() => {
    page = new CustomImageClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
