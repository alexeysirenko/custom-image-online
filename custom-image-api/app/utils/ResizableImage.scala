package utils

import com.sksamuel.scrimage.Image

/**
  * Created by sirenko on 07.03.17.
  */
class ResizableImage(val from: Image) {

  def withResizeActions(actions: ImageResizeAction*) = actions.foldLeft(from)((image, action) => action match {
    case ScaleAction(ratio) => image.scale(ratio)
    case ScaleToAction(width, height) => image.scaleTo(width, height)
    case ScaleToWidthAction(width) => image.scaleToWidth(width)
    case ScaleToHeightAction(height) => image.scaleToHeight(height)
  })

}

object ResizableImage {

  implicit def image2RichImage(from: Image): ResizableImage = new ResizableImage(from)

}