package utils


import java.io.File
import java.nio.file.Files

import org.apache.commons.io.FilenameUtils
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart

import scala.util.{Properties, Random}

/**
  * Created by sirenko on 01.03.17.
  */
object ImageFileHelper {

  private lazy val imagesDir = FilenameUtils.normalize(Properties.tmpDir + File.separator)

  def getImageFile(fileName: String) = new java.io.File(FilenameUtils.concat(imagesDir, fileName))

  def createTempImageFile(temporaryFile : FilePart[TemporaryFile]): File = {
    createTempImageFile(temporaryFile.filename)
  }

  def createTempImageFile(fileName: String): File = {
    val longEnoughPrefix = FilenameUtils.getBaseName(fileName) match {
      case baseName if baseName.length < 3 => baseName + '_' + Random.alphanumeric.take(3 - baseName.length)
      case baseName => baseName + '_'
    }
    val maybeSuffix = FilenameUtils.getExtension(fileName) match {
      case extension if extension.nonEmpty => '.' + extension
      case emptyExtension => null
    }
    File.createTempFile(longEnoughPrefix, maybeSuffix)
  }

}
