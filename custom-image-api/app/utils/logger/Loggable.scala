package utils.logger

import org.slf4j.LoggerFactory

/**
  * Provides logging.
  */
trait Loggable { self =>

  val log = LoggerFactory.getLogger(self.getClass)

}