package utils.response

import play.api.libs.json._
import play.api.mvc.RequestHeader

/**
 * Failure response wrapper
 *
 * @param requestId unique request id
 * @param errors list of errors
 * @param total total number of errors in the response
 */
case class FailureResponse(requestId: Long, errors: List[ApiError], total: Int) {
  def toJson = FailureResponse.asJson(this)
}

/**
 * Api Error instance
 *
 * @param message error description
 * @param args key-value pairs with detailed information on error
 */
case class ApiError(message: String, args: Map[String, String] = Map.empty[String, String])

object FailureResponse {

  /**
   * Json formats.
   */
  implicit val errorFormat = Json.format[ApiError]

  /**
   * Json formats.
   */
  implicit val failureResponseFormat = Json.format[FailureResponse]

  /**
   * Converts failure response into JSON.
   *
   * @param o object to convert into JSON
   * @return JSON representation of the response
   */
  implicit def asJson(o: FailureResponse): JsValue = Json.toJson(o)

  /**
   * Builds failure response from the error message.
   *
   * @param request request entity
   * @param errorMessage error description
   * @return failure response
   */
  def build(request: RequestHeader, errorMessage: String): FailureResponse = {
    FailureResponse(request.id, List(ApiError(errorMessage)), 1)
  }

  /**
   * Builds failure response from the error message and map of variables
   *
   * @param request request entity
   * @param errorMessage error description
   * @param args key-value pairs with detailed information on error
   * @return failure response
   */
  def build(request: RequestHeader, errorMessage: String, args: Map[String, String]): FailureResponse = {
    FailureResponse(request.id, List(ApiError(errorMessage, args)), 1)
  }

  /**
   * Builds failure response from the api error instance.
   *
   * @param request request entity
   * @param error api error instance
   * @return failure response
   */
  def build(request: RequestHeader, error: ApiError): FailureResponse = {
    FailureResponse(request.id, List(error), 1)
  }

  /**
   * Builds failure response from list of api errors.
   *
   * @param request request entity
   * @param errors list of api errors
   * @return failure response
   */
  def build(request: RequestHeader, errors: Iterable[ApiError]): FailureResponse = {
    FailureResponse(request.id, errors.toList, errors.size)
  }
}
