package utils.response

/**
 * Wrapper for meaningful information of a failure.
 *
 * @param code error code
 * @param message error message
 */
case class FailureWrapper(code: Option[Int] = None,
                          message: String) {
  /**
   * Builds Map representation of the object.
   *
   * @return Map representation of the object
   */
  def toMap: Map[String, String] = FailureWrapper.toMap(this)
}

object FailureWrapper {

  /**
   * Constructs a mongo failure wrapper object from the Throwavle.
   *
   * @param e Throwable instance
   * @return mongo failure wrapper
   */
  def apply(e: Throwable): FailureWrapper = e match {
    //case e: DatabaseException => FailureWrapper(e.code, e.message)
    case e: Throwable => FailureWrapper(None, e.getMessage)
  }



  /**
   * Converts failure wrapper to a map.
   *
   * @param fw failure wrapper
   * @return map with errors
   */
  implicit def toMap(fw: FailureWrapper): Map[String, String] = Map(
    "status" -> "error",
    "code" -> fw.code.map(_.toString).getOrElse(""),
    "message" -> fw.message
  )
}
