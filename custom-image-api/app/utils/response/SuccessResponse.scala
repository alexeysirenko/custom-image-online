package utils.response

import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.Request

/**
 * Success response wrapper.
 *
 * @param requestId unique request id
 * @param data response data
 * @param total total number of records available
 * @tparam T response data type
 */
case class SuccessResponse[T](requestId: Long, data: T, total: Option[Int] = None) {

  /**
   * Converts object into JSON.
   *
   * @return
   */
  def toJson(implicit format: Format[T]): JsValue = SuccessResponse.asJson[T](this)
}

object SuccessResponse {

  /**
   * Builds custom JSON formatter for success response wrapper.
   *
   * @tparam T data type
   * @return JSON formatter
   */
  implicit def successResponseFormat[T: Format]: Format[SuccessResponse[T]] =
    ((__ \ "requestId").format[Long] ~
      (__ \ "data").format[T] ~
      (__ \ "total").formatNullable[Int]
      )(SuccessResponse.apply, unlift(SuccessResponse.unapply))

  /**
   * Converts success into JSON.
   *
   * @param o object to convert in JSON
   * @param format json formats
   * @tparam T data type
   * @return JSON representation of the response
   */
  implicit def asJson[T](o: SuccessResponse[T])(implicit format: Format[T]): JsValue = Json.toJson(o)

  /**
   * Builds success response for the single object.
   *
   * @param request request entity
   * @param data response data
   * @tparam T data type
   * @return success response object
   */
  def build[T](request: Request[_], data: T): SuccessResponse[T] = {
    SuccessResponse(request.id, data)
  }

  /**
   * Builds success response for the list of objects.
   *
   * @param request request entity
   * @param data response data
   * @param total total number of elements available
   * @tparam T data type
   * @return success response object
   */
  def build[T](request: Request[_], data: List[T], total: Option[Int] = None): SuccessResponse[List[T]] = {
    SuccessResponse(request.id, data, total)
  }
}
