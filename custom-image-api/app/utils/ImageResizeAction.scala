package utils

/**
  * Created by sirenko on 02.03.17.
  */
sealed trait ImageResizeAction

case class ScaleAction(ratio: Double) extends ImageResizeAction {
  require(ratio > 0 && ratio <= 8, s"Scale ratio $ratio is out of range")
}

case class ScaleToAction(width: Int, height: Int) extends ImageResizeAction {
  val sizeRange = Range(1, 8192).inclusive
  require(sizeRange.contains(width), s"Scale width $width is out of range ${sizeRange.start}-${sizeRange.end}")
  require(sizeRange.contains(height), s"Scale height $height is out of range ${sizeRange.start}-${sizeRange.end}")
}

case class ScaleToWidthAction(width: Int) extends ImageResizeAction {
  val sizeRange = Range(1, 8192).inclusive
  require(sizeRange.contains(width), s"Scale width $width is out of range ${sizeRange.start}-${sizeRange.end}")
}

case class ScaleToHeightAction(height: Int) extends ImageResizeAction {
  val sizeRange = Range(1, 8192).inclusive
  require(sizeRange.contains(height), s"Scale height $height is out of range ${sizeRange.start}-${sizeRange.end}")
}
