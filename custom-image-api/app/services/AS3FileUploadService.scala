package services

import java.io.File

import com.google.inject.Singleton

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by sirenko on 23.03.17.
  */
@Singleton
class AS3FileUploadService {

  def upload(files: List[File]): Future[List[String]] = Future {
    // TODO: STUB. Replace with real AS3 uploading
    files.map(file => s"http://placehold.it/512?text=STUB+IMAGE")
  }

  def upload(file: File): Future[String] = Future {
    // TODO: STUB. Replace with real AS3 uploading
    s"http://placehold.it/512?text=STUB+IMAGE"
  }

}
