package services

import java.io.File
import javax.inject.Inject

import com.google.inject.Singleton
import com.sksamuel.scrimage.Image
import utils.ImageResizeAction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import utils.ResizableImage._

/**
  * A service to resize images.
  */
@Singleton
class ImageResizeService {

  def resizeImage(input: File, output: File, actions: List[ImageResizeAction]): Future[File] = Future {
    Image.fromFile(input).withResizeActions(actions: _*).output(output)
  }

}
