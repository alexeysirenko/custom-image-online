package services

import java.util.UUID

import com.google.inject.Singleton

/**
  * Created by sirenko on 24.03.17.
  */
@Singleton
class ImageRequestService {

  var resizeRequests = Map[String, List[ParsedResizeRequest]]()

  def saveRequest(requests: List[ParsedResizeRequest]): String = {
    val token = UUID.randomUUID().toString
    resizeRequests += (token -> requests)
    token
  }

  def getRequest(token: String): Option[List[ParsedResizeRequest]] = {
    resizeRequests.get(token)
  }

}
