package services

import java.io.File

import com.google.inject.Singleton
import models.json.ImageResizeRequest
import utils._

import scala.util.Try

/**
  * Parses image resize requests.
  */
@Singleton
class ResizeRequestParser {

  def parseRequest(imageResizeRequests: List[ImageResizeRequest]): Try[List[ParsedResizeRequest]] = Try {
    imageResizeRequests map { rawResizeRequest =>
      val imageId = rawResizeRequest.imageId
      val actions: List[ImageResizeAction] = rawResizeRequest.actions map { rawAction =>
        val actionAlias = rawAction.actionName
        val actionParams = rawAction.options
        actionAlias match {
          case "scale" => ScaleAction(actionParams.head)
          case "scaleTo" => ScaleToAction(actionParams.head.toInt, actionParams.last.toInt)
          case "scaleToWidth" => ScaleToWidthAction(actionParams.head.toInt)
          case "scaleToHeight" => ScaleToHeightAction(actionParams.head.toInt)
          case any => throw new Exception(s"Unknown action $any")
        }
      }
      ParsedResizeRequest(imageId, actions)
    }
  }

}

case class ParsedResizeRequest(imageId: String, actions: List[ImageResizeAction])
