package controllers

import javax.inject.Inject
import models.json.{ImageResizeRequest, ImageResizeResponse, UploadedImage}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.mvc._
import services._
import utils.ImageFileHelper
import utils.response.{FailureResponse, FailureWrapper, SuccessResponse}

import scala.concurrent.Future
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

class ImageResizeController @Inject()(imageResizingService: ImageResizeService,
                                      resizeRequestParser: ResizeRequestParser,
                                      imageRequestService: ImageRequestService,
                                      aS3FileUploadService: AS3FileUploadService) extends Controller {

  def getResizeToken = Action(parse.json) { request =>
    request.body.asOpt[List[ImageResizeRequest]] map { rawResizeRequests =>
      val maybeParsedResizeRequests = resizeRequestParser.parseRequest(rawResizeRequests)
      maybeParsedResizeRequests match {
        case Failure(NonFatal(e)) =>
          BadRequest[JsValue](FailureResponse.build(request, s"Failed to parse action parameters", FailureWrapper(e)))
        case Success(parsedResizeRequests) =>
          val token = imageRequestService.saveRequest(parsedResizeRequests)
          Ok[JsValue](SuccessResponse(request.id, data = token).toJson)
      }
    } getOrElse {
      BadRequest[JsValue](FailureResponse.build(request, "Failed to parse image resize request"))
    }
  }

  def resizeImages(resizeToken: String) = Action.async(parse.multipartFormData) { request =>
    Logger.debug(s"Resizing ${request.body.files.length} image(s)...")
    imageRequestService.getRequest(resizeToken) match {
      case None => Future.successful(BadRequest[JsValue](FailureResponse.build(request, s"Resize request with token $resizeToken not found")))
      case Some(resizeRequests) =>
        val maybeResizedImages = resizeRequests map { resizeRequest =>
          val maybeFilePart = request.body.file(resizeRequest.imageId)
          maybeFilePart match {
            case None => Future.failed(new Exception(s"Missing image with id '${resizeRequest.imageId}' in uploaded image files"))
            case Some(filePart) =>
              val tempImageFile = filePart.ref.moveTo(ImageFileHelper.createTempImageFile(filePart), replace = true)
              val outputImageFile = ImageFileHelper.createTempImageFile(s"resized_${tempImageFile.getName}")
              val maybeResizedImages = imageResizingService.resizeImage(tempImageFile, outputImageFile, resizeRequest.actions) map { resizedImageFile =>
                (resizeRequest.imageId, resizedImageFile)
              } recoverWith { case NonFatal(e) => Future.failed(new Exception(s"Failed to resize an image", e)) }
              maybeResizedImages
          }
        }
        val maybeUploadedImages = maybeResizedImages map { maybeResizedPairs =>
          maybeResizedPairs flatMap  { case (imageId, resizedImageFile) =>
            aS3FileUploadService.upload(resizedImageFile) map { uploadedImageUrl =>
              UploadedImage(imageId, uploadedImageUrl)
            } recoverWith { case NonFatal(e) => Future.failed(new Exception(s"Failed to upload an image", e)) }
          }
        }
        Future.sequence(maybeUploadedImages) map { uploadedImages =>
          Ok[JsValue](SuccessResponse(request.id, data = ImageResizeResponse(resizeToken, uploadedImages)).toJson)
        } recover { case NonFatal(e) =>
          InternalServerError[JsValue](FailureResponse.build(request, "Failed to resize images", FailureWrapper(e)))
        }
    }
  }

}
