package models.json

import play.api.libs.json.Json

/**
  * Created by sirenko on 24.03.17.
  */
case class UploadedImage(imageId: String, downloadUrl: String)
object UploadedImage {
  implicit val format = Json.format[UploadedImage]
}

case class ImageResizeResponse(token: String, images: List[UploadedImage])
object ImageResizeResponse {
  implicit val format = Json.format[ImageResizeResponse]
}