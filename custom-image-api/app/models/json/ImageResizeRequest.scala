package models.json

import java.io.File

import play.api.libs.json.Json

/**
  * Created by sirenko on 16.03.17.
  */
case class ImageResizeAction(actionName: String, options: List[Double])
object ImageResizeAction {
  implicit val format = Json.format[ImageResizeAction]
}

case class ImageResizeRequest(imageId: String, actions: List[ImageResizeAction])
object ImageResizeRequest {
  implicit val format = Json.format[ImageResizeRequest]
}



