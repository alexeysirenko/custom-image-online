name := """custom-image-api"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"



lazy val root = project.in(file(".")).enablePlugins(PlayScala)

libraryDependencies += filters

libraryDependencies ++= Seq(
  "commons-io" % "commons-io" % "2.5",
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.7",
  "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.7",
  "com.sksamuel.scrimage" %% "scrimage-filters" % "2.1.7"
)
